#!/bin/sh

exec > README.html
cat <<EOF
<html>
<head>
  <title>EasyCluster documentation</title>
  <style type="text/css">
    body {
      font-family: sans-serif;
    }
    h1, h2, h3 {
      background-color: #f2f2f2;
      font-weight: bold;
      border-bottom: 1px solid #eee;
    }
    h2, h3 {
      margin-top: 1.5em;
    }
    pre {
      margin-left: 30px;
      margin-right: 30px;
      background-color: #f8f8f8;
      border: 1px solid #ddd;
      border-radius: 3px 3px 3px 3px;
    }
    p > code {
      background-color: #f8f8f8;
      border: 1px solid #ddd;
      border-radius: 3px 3px 3px 3px;
    }
  </style>
</head>
<body>
EOF

markdown README.md

cat <<EOF
</body>
</html>
EOF
